package com.example.problem;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.problem.web.UserController;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class ProblemApplicationTests {
	
	@Autowired
	UserController userController;

	@Autowired
	HttpServletResponse[][] response;
	
//	@Test
//	public void bambambam() throws Exception{
//		// hardware optimization
//		System.out.println("Test Starts...");
//			for(int partition = 10; partition <= 20; partition++){
//				for(int experiment = 0; experiment < 5; experiment++){
//					System.out.println("Testing... Partition : "+partition+" - File Size : "+25+" - Experiment : "+experiment);
//					userController.test(response[partition-10][experiment], (int)java.lang.Math.pow(2.0, partition), 25, experiment, true);
//					System.gc();
//				}
//			}
//	}
	
}
