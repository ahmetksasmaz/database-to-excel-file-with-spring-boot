package com.example.problem.model;

public class Stat {

	private long maxUsedMem = 0;
	private double avgUsedMem = 0;
	private long excelTime = 0;
	private int partition = 0;
	
	public long getMaxUsedMem() {
		return maxUsedMem;
	}
	public void setMaxUsedMem(long maxUsedMem) {
		this.maxUsedMem = maxUsedMem;
	}
	public double getAvgUsedMem() {
		return avgUsedMem;
	}
	public void setAvgUsedMem(double avgUsedMem) {
		this.avgUsedMem = avgUsedMem;
	}
	public long getExcelTime() {
		return excelTime;
	}
	public void setExcelTime(long excelTime) {
		this.excelTime = excelTime;
	}
	public int getPartition() {
		return partition;
	}
	public void setPartition(int partition) {
		this.partition = partition;
	}
	@Override
	public String toString() {
		return maxUsedMem+","+avgUsedMem+","+excelTime+","+partition;
	}
	
	
	
}
