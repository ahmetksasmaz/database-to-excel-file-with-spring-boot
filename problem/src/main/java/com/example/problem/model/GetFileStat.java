package com.example.problem.model;

public class GetFileStat {

	private long memory;
	
	public GetFileStat() {
		
	}
	
	public GetFileStat(long memory) {
		this.memory = memory;
	}
	
	public long getMemory() {
		return memory;
	}
	
	public void setMemory(long memory) {
		this.memory = memory;
	}
	
	
	
}
