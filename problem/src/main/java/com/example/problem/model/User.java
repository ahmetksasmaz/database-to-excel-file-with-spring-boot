package com.example.problem.model;

public class User{

	private String username;
	private String password;
	private String username2;
	private String password2;
	private String username3;
	private String password3;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	public String getUsername2() {
		return username2;
	}
	public void setUsername2(String username2) {
		this.username2 = username2;
	}
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	public String getUsername3() {
		return username3;
	}
	public void setUsername3(String username3) {
		this.username3 = username3;
	}
	public String getPassword3() {
		return password3;
	}
	public void setPassword3(String password3) {
		this.password3 = password3;
	}
	public Object[] toArray(){
		Object[] array = new Object[6];
		array[0] = this.username;
		array[1] = this.password;
		array[2] = this.username2;
		array[3] = this.password2;
		array[4] = this.username3;
		array[5] = this.password3;
		return array;
	}
	
}
