package com.example.problem.web;

import org.springframework.web.bind.annotation.RestController;

import com.example.problem.model.Stat;
import com.example.problem.service.UserService;
import com.gc.iotools.stream.is.TeeInputStreamOutputStream;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.swing.SwingUtilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.GET , value = "/getDatabase")
	public ResponseEntity<?> getDatabase() throws InterruptedException{
		System.out.println("Service started.");
		for(int i = 0; i < 25; i++){
			System.gc();
			System.gc();
			Thread.sleep(1000);
			long start_time = (new Date()).getTime();
			long start_mem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
			List<Object[]> users = userService.getUsers();
			long end_time = (new Date()).getTime();
			long end_mem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
			
			System.out.println((end_time-start_time)+","+(end_mem-start_mem));
		}
		System.out.println("Service ended.");
		
		return ResponseEntity.ok("");
	}
	
	@GetMapping("/getExcel/{fileSize}/{experiment}")
	public void test(HttpServletResponse response, @PathVariable("fileSize") int fileSize, @PathVariable("experiment") int experiment) throws IOException {
		System.gc();
		
		FileWriter fw = new FileWriter("/home/harezmi/Desktop/logStatistics.txt", true);
	    BufferedWriter bw = new BufferedWriter(fw);
	    PrintWriter out = new PrintWriter(bw);
		
	    Timer timer = new Timer("Display Timer");
		long start_time = (new Date()).getTime();
		long start_mem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
	    
    	out.println("0,"+start_mem+","+fileSize+","+experiment);
		
		
		TimerTask task = new TimerTask() {
		    @Override
		    public void run() {
		        // Task to be executed every second
		        try {
		            SwingUtilities.invokeAndWait(new Runnable() {
		                @Override
		                public void run() {
		                		Date date = new Date();
			                	out.println((date.getTime()-start_time)+","+(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()-start_mem)+","+fileSize+","+experiment);
		                }
		            });
		        } catch (InvocationTargetException | InterruptedException e) {
		            e.printStackTrace();
		        }
		    }
		};
		
		
		timer.scheduleAtFixedRate(task, 100, 100);
		
		String fileName = "/home/harezmi/Desktop/"+fileSize+"MB";
		
		File file = new File(fileName);
        
        response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.addHeader("Pragma", "no-cache");
        response.addHeader("Expires", "0");
        response.addHeader("content-disposition", "attachment; filename=\"file"+fileSize+"_"+experiment+".bin\"");
		response.setContentLength((int)file.length());
		response.setContentType("application/octet-stream");
		
        InputStream inputStream = new FileInputStream(file);
        ServletOutputStream outputStream = response.getOutputStream();
        
        TeeInputStreamOutputStream tee=new TeeInputStreamOutputStream(inputStream,outputStream);
//        org.apache.commons.io.IOUtils.copy(tee,outputStream);
//        int buffer_size = (int)(((double)file.length())/java.lang.Math.pow(2.0, partition));
//        byte[] buffer = new byte[buffer_size];
//        int b = -1;
//        while((b = inputStream.read(buffer, 0, buffer_size)) !=-1) {
//        	outputStream.write(buffer, 0 , b);
//        }
        
//        inputStream.close();
//        outputStream.close();
        tee.close();
        
        timer.cancel();
        
        
        	Date date = new Date();
        	out.println((date.getTime()-start_time)+","+(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()-start_mem)+","+fileSize+","+experiment);
        
        out.close();
        
	}
	
	@RequestMapping(method = RequestMethod.GET , value = "/getUsers")
	public ResponseEntity<?> getUsers() throws FileNotFoundException, IOException{
		
		List<Object[]> users = userService.getUsers();
		long start_mem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println(userService.listUsers(100 , start_mem , users));
		
//		Stat[][] stat = new Stat[1][121];
//		int[] partition_list = new int[]{1};
//		Stat[] stat_data = new Stat[]{new Stat()};
//		
//		List<Object[]> users = userService.getUsers();
//		
//		for(int i = 0; i < 1; i++){
//			stat_data[i].setPartition(partition_list[i]);
//			for(int j = 0; j < 121; j++){
//				System.gc();
//				long start_mem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
//				
//				stat[i][j] = userService.listUsers(partition_list[i] , start_mem , users);
//				stat_data[i].setAvgUsedMem(stat_data[i].getAvgUsedMem()+stat[i][j].getAvgUsedMem());
//				stat_data[i].setExcelTime(stat_data[i].getExcelTime()+stat[i][j].getExcelTime());
//				stat_data[i].setMaxUsedMem(stat_data[i].getMaxUsedMem()+stat[i][j].getMaxUsedMem());
//				
//				System.out.println(stat[i][j]);
//				
//			}
//		}
//		
//		for(int i = 0; i < 1; i++){
//			System.out.println("Partition : "+stat_data[i].getPartition()+"| Avg : "+stat_data[i]);
//		}
		
		return ResponseEntity.ok("");
	}
	
	@RequestMapping(method = RequestMethod.GET , value = "/create")
	public ResponseEntity<?> create() throws FileNotFoundException, IOException{
		System.out.println("Service started.");
		userService.create();
		System.out.println("Service ended.");
		
		return ResponseEntity.ok("");
	}
	
}
