package com.example.problem.dao;

import java.util.List;

import com.example.problem.model.User;

public interface UsersRepository {

		List<Object[]> findAll();
		void create();
		
}
