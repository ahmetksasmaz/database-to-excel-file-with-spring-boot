package com.example.problem.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.problem.dao.UsersRepository;

@Repository
public class UsersJdbcImpl implements UsersRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private RowMapper<Object[]> rowMapper = new RowMapper<Object[]>() {

		@Override
		public Object[] mapRow(ResultSet rs, int rowNum) throws SQLException {
			Object[] obje = new Object[6];
			
			obje[0] = rs.getString("username");
			obje[1] = rs.getString("password");
			obje[2] = rs.getString("username2");
			obje[3] = rs.getString("password2");
			obje[4] = rs.getString("username3");
			obje[5] = rs.getString("password3");
			
			return obje;
		}
	};
	
	@Override
	public List<Object[]> findAll() {
		String sql = "select username,password,username2,password2,username3,password3 from problem_users";
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public void create() {
		for(int i = 0; i < 175000; i++){
			jdbcTemplate.execute("insert into problem_users values('sdfasdfasdfasfqcwyhebfgcquywecgfqıwueycfgqnwıubqefqnw"+i+"_1','sdfasdfasdfasfqcwyhebfgcquywecgfqıwueycfgqnwıubqefqnw"+i+"_2','sdfasdfasdfasfqcwyhebfgcquywecgfqıwueycfgqnwıubqefqnw"+i+"_3','sdfasdfasdfasfqcwyhebfgcquywecgfqıwueycfgqnwıubqefqnw"+i+"_4','sdfasdfasdfasfqcwyhebfgcquywecgfqıwueycfgqnwıubqefqnw"+i+"_5','sdfasdfasdfasfqcwyhebfgcquywecgfqıwueycfgqnwıubqefqnw"+i+"_6')");
		}
	}
	
}
