package com.example.problem.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.problem.dao.UsersRepository;
import com.example.problem.model.Stat;
import com.example.problem.model.User;

@Service
public class UserServiceImpl implements UserService {

	private UsersRepository usersRepository;
	
	@Autowired
	public void setUsersRepository(UsersRepository usersRepository) {
		this.usersRepository = usersRepository;
	}

	
	
	@Override
	public List<Object[]> getUsers() {
		return usersRepository.findAll();
	}


	
	

	@Override
	public void create() {
		usersRepository.create();
	}



	@Override
	public Stat listUsers(int partition, long startMem , List<Object[]> users) throws FileNotFoundException, IOException {
		Date date = null;
		
		String fileName = "/home/harezmi/Desktop/problem.xlsx";
		
		
		// STREAM FILE WRITE STARTS

		int rowCount = users.size();
		
		date = new Date();
		long excel_start = date.getTime();
		
		long[] usedMem = new long[rowCount];
		
		SXSSFWorkbook wb = new SXSSFWorkbook();
		SXSSFSheet sh = wb.createSheet();
		Row row = null;
		Object[] user = null;
		for(int rownum = 0; rownum < rowCount; rownum++){
			row = sh.createRow(rownum);
			user = users.get(rownum);
			row.createCell(0).setCellValue((String)user[0]);
			row.createCell(1).setCellValue((String)user[1]);
			row.createCell(2).setCellValue((String)user[2]);
			row.createCell(3).setCellValue((String)user[3]);
			row.createCell(4).setCellValue((String)user[4]);
			row.createCell(5).setCellValue((String)user[5]);
//			if(rownum % 1000 == 0){sh.flushRows(1000);}
			
			usedMem[rownum] = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory() - startMem;
			System.out.println(rownum+"-->"+usedMem[rownum]);
		}
		
		OutputStream out = new BufferedOutputStream(new FileOutputStream(fileName));
		wb.write(out);
		out.close();
		
//		wb.dispose();
		wb.close();
		
		date = new Date();
		long excel_end = date.getTime();
		
		// STREAM FILE WRITE ENDS
		
		long maxUsedMem = getMax(usedMem);
		double avgUsedMem = getAvg(usedMem);
		
		Stat stat = new Stat();
		
		stat.setMaxUsedMem(maxUsedMem);
		stat.setAvgUsedMem(avgUsedMem);
		stat.setExcelTime(excel_end-excel_start);
		stat.setPartition(partition);
		
        return stat;
        
	}
	
	public long getMax(long[] inputArray){ 
	    long maxValue = inputArray[0]; 
	    for(int i=1;i < inputArray.length;i++){ 
	      if(inputArray[i] > maxValue){ 
	         maxValue = inputArray[i]; 
	      } 
	    } 
	    return maxValue; 
	 }
	
	public static double getAvg(long[] inputArray) {
        long total = 0;

        for(int i=0; i<inputArray.length; i++){
        	total = total + inputArray[i];
        }
        
        double average = total / inputArray.length;
        
        return average;
    }

}
