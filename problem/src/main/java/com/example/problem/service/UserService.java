package com.example.problem.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.example.problem.model.Stat;

public interface UserService {

	public Stat listUsers(int partition, long startMem , List<Object[]> users) throws FileNotFoundException, IOException;
	public List<Object[]> getUsers();
	public void create();
}
